# Covid19Outcomes Data Calculations
This repository contains the code that estimates case fatality rate by age and gender (cohort) as well as some 
other statistics.

You can find the deployed web application that uses this data at 
[https://korea.covid19snapshot.org/](https://korea.covid19snapshot.org/)

## References
The following data and work has been references/adapted and used while doing these calculations.

### Source Data
The data being used in these calculations are retrieved and updated daily from  
[this](https://www.kaggle.com/kimjihoo/coronavirusdataset) kaggle data set created and maintained by *datartist*. We
access the [github repository](https://github.com/jihoo-kim/Coronavirus-Dataset) where the data is mirrored.

We used [data](http://27.101.213.4/index.jsp) published by the South Korean Ministry of Public Administration 
and Securities for the population statistics.

We also used [data](http://kosis.kr/statHtml/statHtml.do?orgId=101&tblId=DT_1B34E01&vw_cd=MT_ZTITLE&list_id=D11&seqNo=&lang_mode=ko&language=kor&obj_var_id=&itm_id=&conn_path=MT_ZTITLE) 
published by Korean Statistical Information Service for the statistics on causes of death.

### Comorbidity Data
Comorbidity data is not yet available in details for South Korea. We referenced 
[this](https://www.thelancet.com/journals/lancet/article/PIIS0140-6736(20)30566-3/fulltext) Lancet journal article for
cormobidity data to factor into our calculations. 

### Calculations

The calculations used to estimate the Case fatality rate has been adapted from 
[this](https://www.kaggle.com/marcuswingen/analysis-of-covid-19-data-from-south-korea) kaggle kernel by *Marcus Wingen* 

#### CFR and Estimated CFR

The overall CFR is calculated by 

`Total Deceased / Total Confirmed Patients * 100` and then rounded to the first decimal point.

The age and gender data is incomplete at the moment, so we get the current distribution of the age and gender and
apply it to the total patients to get the estimated patient counts and estimated deceased counts by age and gender.

This is the estimated CFR for a gender and age group.

#### Above 70 default CFR

Some age groups above 70 years doesn't have any reported mortalities, however based on the general trend in data
is it obvious that older you get, the higher changes of death become.

We did want to reflect this reality, and thus if an age group doesn't have any mortalities (resulting in a 0 CFR) we
get the highest CFR after 70 years and apply it to the age group.

#### Comorbidity Factoring

Comorbidity factors are extracted from the linked Lancet journal article and applied to the CFR of a cohort.

The comorbidity is applied in the [front end consumer application](https://gitlab.com/favoritemedium-oss/fm-covid-web-client/-/blob/master/README.md)

The calculation in use is simple, get the estimated CFR for the cohort(age and gender group) and multiply if by the
comorbidity factor

`Estimated CFR * Comorbidity Factor`

## Deployment

This is a simple app, and the data changes/updates once per day. We deployed this code as a Google Cloud function, and
write the calculated statistics as a json file into cloud storage.

The cloud function is triggered everyday at the first minute after midnight.

The files are named after the date it was generated. A fallback file named general is kept updated, the idea is for
any consuming application to fall back to that file is the current date's file is unavailable due to a bug.

The json files in storage are publicly accessible.

A sample looks like this

`https://storage.googleapis.com/covid9stats/covid9stats/2020_03_26_data.json`
`https://storage.googleapis.com/covid9stats/covid9stats/latest_data.json`