import datetime

from google.cloud import storage


def upload_file(data):
	client = storage.Client()
	bucket = client.get_bucket("covid9stats")

	date = datetime.datetime.utcnow().strftime("%Y_%m_%d")

	date_file = bucket.blob(f"covid9stats/{date}_data.json")
	date_file.upload_from_string(data, predefined_acl="publicRead")

	latest_file = bucket.blob(f"covid9stats/latest_data.json")
	latest_file.upload_from_string(data, predefined_acl="publicRead")
