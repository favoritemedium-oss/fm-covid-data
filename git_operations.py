import os
from dulwich import porcelain

repo_path = f"/tmp/Coronavirus-Dataset"
repo_url = "https://github.com/jihoo-kim/Coronavirus-Dataset"


def update_git():
	if os.path.isdir(repo_path):
		porcelain.pull(repo_path, repo_url, "master", errstream=porcelain.NoneStream(),
		               outstream=porcelain.NoneStream())
	else:
		porcelain.clone(repo_url, repo_path, errstream=porcelain.NoneStream(), outstream=porcelain.NoneStream())
