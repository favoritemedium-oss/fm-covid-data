import json
import math
import datetime
import numpy as np
import pandas as pd
import seaborn as sns

pd.plotting.register_matplotlib_converters()
sns.set_style("whitegrid")
pd.set_option("display.max_columns", 30)

# load data
patient_path = "/tmp/Coronavirus-Dataset/dataset/Patient/PatientInfo.csv"
time_path = "/tmp/Coronavirus-Dataset/dataset/Time/Time.csv"
route_path = "/tmp/Coronavirus-Dataset/dataset/Patient/PatientRoute.csv"


def group_age(age):
	"""This function is used to group patients by age
	in steps of 10 years. It returns the age range
	of the patient as a string.
	"""
	if age >= 0:  # not NaN
		if age % 10 != 0:
			lower = int(math.floor(age / 10.0)) * 10
			upper = int(math.ceil(age / 10.0)) * 10 - 1
			return f"{lower}-{upper}"
		else:
			lower = int(age)
			upper = int(age + 9)
			return f"{lower}-{upper}"
	return "Unknown"


def calculate_statistics():
	patient = pd.read_csv(patient_path, index_col="patient_id")
	patient.info()
	time = pd.read_csv(time_path, index_col="date")
	route = pd.read_csv(route_path, index_col="patient_id")

	# load overall population breakdown
	with open('overall_population.json', 'r') as causes_file:
		population_data = json.load(causes_file)

	# load overall population breakdown
	with open('causes_of_death.json', 'r') as causes_file:
		months_since_jan = datetime.datetime.utcnow().month

		causes_of_death_by_age = json.load(causes_file)

		for age_group, mortalities in causes_of_death_by_age.items():
			for mortality in mortalities.keys():
				mortalities[mortality] = mortalities[mortality] * months_since_jan / 12

	print(f"Last Update: {datetime.datetime.today().strftime('%m/%d/%Y')}")
	# format date columns:
	date_cols = ["confirmed_date", "released_date", "deceased_date"]
	for col in date_cols:
		patient[col] = pd.to_datetime(patient[col])
	time.index = pd.to_datetime(time.index)
	# correct single spelling mistake:
	patient.loc[patient["sex"] == "feamle", "sex"] = "female"
	# correct single empty birth_year entry
	patient.loc[patient["birth_year"] == " ", "birth_year"] = np.nan
	patient["birth_year"] = patient["birth_year"].astype("float")
	# Derive features:
	# status by gender:
	patient["state_by_gender"] = patient["state"] + "_" + patient["sex"]
	# age:
	# approximation, using 2019 - assuming 3/4 are born after march/current month of 2020
	patient["age"] = 2019 - patient["birth_year"]

	patient["age_range"] = patient["age"].apply(group_age)
	# duration of infection:
	patient["time_to_release_since_confirmed"] = patient["released_date"] - patient["confirmed_date"]
	patient["time_to_death_since_confirmed"] = patient["deceased_date"] - patient["confirmed_date"]
	patient["duration_since_confirmed"] = patient[
		["time_to_release_since_confirmed", "time_to_death_since_confirmed"]].min(
		axis=1)
	patient["duration_days"] = patient["duration_since_confirmed"].dt.days

	# for case fatality rate:
	patient["state_deceased"] = (patient["state"] == "deceased").astype("int8")

	# for underlying diseases:
	patient.loc[patient["disease"] == 1, "disease"] = "Underlying disease"
	patient.loc[patient["disease"] == 0, "disease"] = "No underlying disease"
	patient.loc[patient["disease"].isna(), "disease"] = "Unknown"
	total_confirmed = time.sort_values(by="date", ascending=False).iloc[0]["confirmed"]
	total_deceased = time.sort_values(by="date", ascending=False).iloc[0]["deceased"]
	total_recovered = time.sort_values(by="date", ascending=False).iloc[0]["released"],
	total_cfr = round((total_deceased / total_confirmed * 100), 1)
	print(f"The current CFR for COVID-19 in South Korea is {total_cfr} %."),
	print(f"This number is based on {total_confirmed} confirmed cases and {total_deceased} fatalities."),

	# rate by gender and age:
	cfr_gender_age = pd.DataFrame(patient.groupby(["age_range", "sex"])["state_deceased"].describe()[["count", "mean"]])
	cfr_gender_age.rename(columns={"count": "Number of patients", "mean": "CFR"}, inplace=True)
	cfr_gender_age.drop("Unknown", axis=0, inplace=True)

	# formatting:
	cfr_gender_age["Number of patients"] = cfr_gender_age["Number of patients"].astype("int64")
	cfr_gender_age["CFR"] = round(cfr_gender_age["CFR"], 3) * 100
	cfr_gender_age.rename(columns={"CFR": "CFR [%] (gender and age known)"}, inplace=True)

	# only for plotting:
	cfr_gender_age["age_range"] = list(x[0] for x in cfr_gender_age.index)
	cfr_gender_age["gender"] = list(x[1] for x in cfr_gender_age.index)

	# extrapolation
	fraction_age_known = patient.loc[patient["age"].notna()].shape[0] / total_confirmed
	fraction_sex_known = patient.loc[patient["sex"].notna()].shape[0] / total_confirmed
	cfr_gender_age["Estimated total number of patients"] = (
		cfr_gender_age["Number of patients"] / fraction_age_known).astype("int64")
	cfr_gender_age["Estimated total CFR [%]"] = round(
		(cfr_gender_age["CFR [%] (gender and age known)"] * fraction_age_known), 1)

	print(cfr_gender_age)

	rows = [row for index, row in cfr_gender_age.iterrows()]
	rows.sort(key=lambda x: int(x["age_range"].split("-")[0]))

	age_sex_data = {}
	largest_cfr_70_plus = 0.0
	largest_estimated_cfr_70_plus = 0.0

	for row in rows:
		age_range = row["age_range"]
		range_start = int(age_range.split("-")[0])
		if age_range not in age_sex_data:
			if range_start >= 90:
				# Causes of death above 90 are concatenated into a single group in the available data
				causes_of_death = causes_of_death_by_age["90+"]
			else:
				causes_of_death = causes_of_death_by_age[age_range]

			age_sex_data[age_range] = {"male": None, "female": None,
			                           "mortality_stats_per_100k": causes_of_death}

		if range_start >= 100:
			# Population data above 100 years are concatenated into a single group
			population_count = population_data["100+"][row["gender"]]
		else:
			population_count = population_data[age_range][row["gender"]]

		cfr_known = row["CFR [%] (gender and age known)"]
		estimated_patients = row["Estimated total number of patients"]
		cfr_estimated = row["Estimated total CFR [%]"]

		# If the cfr is not available we don't want to provide a 0.0 cfr value for age groups
		# above 70 years old, as this gives a false sense of security for a group that's at risk more
		if range_start >= 80 and cfr_known == 0.0:
			cfr_known = largest_cfr_70_plus
			cfr_estimated = largest_estimated_cfr_70_plus

		if range_start >= 70 and cfr_known > largest_cfr_70_plus:
			largest_cfr_70_plus = cfr_known
			largest_estimated_cfr_70_plus = cfr_estimated

		estimated_deceased_patients = estimated_patients * cfr_estimated / 100

		age_sex_data[age_range][row["gender"]] = {
			"cfr_known": cfr_known,
			"patient_count_known": row["Number of patients"],
			"patient_count_estimated": estimated_patients,
			"estimated_mortality": estimated_deceased_patients,
			"cfr_estimated": cfr_estimated,
			"mortality_per_100k": estimated_deceased_patients / population_count * 100000,
			"total_population": population_count,
			"percentage_infected": estimated_patients / population_count * 100
		}

	results = {"age_sex_breakdown": age_sex_data, "default_cfr": total_cfr}
	return results
