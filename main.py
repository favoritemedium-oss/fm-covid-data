import json

from cloud_storage import upload_file
from data_analyzer import calculate_statistics
from git_operations import update_git


def generate_stats_and_upload(request):
	update_git()

	stats = calculate_statistics()

	# load overall comorbidity data
	with open('comorbidity.json', 'r') as comorbidity_file:
		comorbidity = json.load(comorbidity_file)

	stats["comorbidity"] = comorbidity

	# upload_file(json.dumps(stats))
	print(json.dumps(stats))
	return "Stats generated successfully."

generate_stats_and_upload(None)